from collections import Counter
import time

def import_textfile(filename):
    print 'IMPORT'
    t0 = time.time()
    
    #Function
    inputfile = open(filename,'r')
    text = inputfile.read().strip().split('\n')
    size = inputfile.tell()
    line_cnt = len(text)
    inputfile.close()
    
    #Stats
    t1 = time.time()
    t = t1-t0
    print '  lines: '+str(line_cnt)
    print '  size: '+str(size)+' bytes'
    print '  process time: '+str(t)+' seconds'
    print '  time/line: '+str(t/line_cnt)
    print '  time/byte: '+str(t/size)
    return text
    
def make_lowercase(lines):
    print 'LOWERCASE'
    t0 = time.time()
    
    #Function
    linesLow = [line.lower() for line in lines]
    line_cnt = len(linesLow)
    
    #Stats
    t1 = time.time()
    t = t1-t0
    print '  lines: '+str(line_cnt)
    print '  process time: '+str(t)+' seconds'
    print '  time/line: '+str(t/line_cnt)
    return linesLow
    
def count_sentences(lines):
    print 'COUNT'
    t0 = time.time()
    
    #Function
    freqs = Counter(lines)
    key_count = len(freqs)
    
    #Stats
    t1 = time.time()
    t = t1-t0
    print '  unique keys: '+str(key_count)    
    print '  process time: '+str(t)+' seconds'
    print '  time/key: '+str(t/key_count)
    return freqs
    
def sort_frequencies(freqs):
    print 'SORT'
    t0 = time.time()
    
    #Function
    sorted_freqs = freqs.most_common()
    key_count = len(sorted_freqs)
    
    #Stats
    t1 = time.time()
    t = t1-t0
    print '  unique keys: '+str(key_count)
    print '  process time: '+str(t)+' seconds'
    print '  time/key: '+str(t/key_count)
    return sorted_freqs

def output_frequency_data(freqs):
    print 'OUTPUT'
    t0 = time.time()
    
    #Function
    outputfile = open('output.txt','w')
    outputfile.write('Sentence : Count\n')
    for pair in freqs:
        key = pair[0]
        freq = pair[1]
        pair = key+' : '+str(freq)+'\n'
        outputfile.write(pair)
    key_cnt = len(freqs)
    size = outputfile.tell()
    outputfile.close()
    
    #Stats
    t1 = time.time()
    t = t1-t0
    print '  size: '+str(size)+' bytes'
    print '  unique keys: '+str(key_cnt)
    print '  process time: '+str(t)+' seconds'
    print '  time/key: '+str(t/key_cnt)
    print '  time/byte: '+str(t/size)
    return
    
def main():
    filename = 'input.txt'
    linesRaw = import_textfile( filename )
    linesLow = make_lowercase( linesRaw )
    freqs = count_sentences( linesLow )
    sorted_freqs = sort_frequencies( freqs )
    output_frequency_data( sorted_freqs )

t0 = time.time()
main()
t1 = time.time()
t = t1-t0
print 'TOTAL TIME: '+str(t)




        

        

